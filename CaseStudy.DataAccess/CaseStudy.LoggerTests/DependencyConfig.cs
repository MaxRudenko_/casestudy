﻿using CaseStudy.Logger;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CaseStudy.LoggerTests
{
    public static class DependencyConfig
    {
        public static IServiceCollection GetMockDependencies()
        {
            var services = new ServiceCollection();
            
            services.AddLoggerDependencies("UnitTests",);
            return services;
        }
    }
}
