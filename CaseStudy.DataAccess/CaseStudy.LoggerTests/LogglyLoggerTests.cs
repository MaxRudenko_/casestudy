﻿using CaseStudy.Logger;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CaseStudy.LoggerTests
{
    [TestClass]
    public class LogglyLoggerTests
    {
        private ILogger _logger;

        public LogglyLoggerTests()
        {
            var services = DependencyConfig.GetMockDependencies();
            var serviceProvider = services.BuildServiceProvider();
            _logger = serviceProvider.GetService<ILogger>();
        }

        [TestMethod]
        public void WriteInformationTest()
        {
            _logger.WriteInformation("WriteInformationTest!");
        }

        [TestMethod]
        public void WriteFatalTest()
        {
            try
            {
                throw new Exception("Fatal Error!");
            }
            catch (Exception exception)
            {
                _logger.WriteCritical("WriteFatalTest!", exception);
            }
        }
    }
}
