﻿using CaseStudy.DataAccess.Database;
using CaseStudy.DataAccess.Repositories.Interfaces;
using CaseStudy.Entities;
using CaseStudy.Logger;
using CaseStudy.Services;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CaseStudy.ServicesTests
{
    public static class DependencyConfig
    {
        public static IServiceCollection GetMockDependencies()
        {
            var services = new ServiceCollection();
           
            var mockApplicationContext = new Mock<IApplicationContext>();

            var queryable = new List<Salary>().AsQueryable();
            var dbSet = new Mock<DbSet<Salary>>();
            dbSet.As<IQueryable<Salary>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<Salary>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<Salary>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<Salary>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            mockApplicationContext.Object.Salaries = dbSet.Object;
            
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(x => x.WriteCritical(It.IsAny<string>(), It.IsAny<Exception>(), null, null));
            mockLogger.Setup(x => x.WriteError(It.IsAny<string>(), It.IsAny<Exception>(), null, null));
            mockLogger.Setup(x => x.WriteInformation(It.IsAny<string>(), null, null));
            var mockSalaryRepository = new Mock<ISalaryRepository>();

            mockSalaryRepository.Setup(x => x.Insert(It.IsAny<ICollection<Salary>>()));

            services.AddSingleton(mockApplicationContext.Object);
            services.AddSingleton(mockLogger.Object);
            services.AddSingleton(mockSalaryRepository.Object);

            services.AddServicesDependencies();
            return services;
        }
    }
}
