﻿using CaseStudy.Services.Exceptions;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.ViewModels.SalaryViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CaseStudy.ServicesTests.ServicesTests
{
    [TestClass]
    public class SalaryServiceTests
    {
        private ISalaryService _salaryService;
        public SalaryServiceTests()
        {
            var services = DependencyConfig.GetMockDependencies();
            var serviceProvider = services.BuildServiceProvider();
            _salaryService = serviceProvider.GetService<ISalaryService>();
        }

        [TestMethod]
        public void SetSalaryOutOfRangeTest()
        {
            var viewModel = new SetSalaryRequestViewModel();
            viewModel.Salaries.Add(new SetSalaryRequestViewModelItem()
            {
                Amount = 600
            });
            try
            {
                _salaryService.Set(viewModel).GetAwaiter().GetResult();
            }
            catch (ValidationCustomException)
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.Fail();
        }

        [TestMethod]
        public void SetSalaryAdditionalDataTest()
        {
            var viewModel = new SetSalaryRequestViewModel();
            viewModel.Salaries.Add(new SetSalaryRequestViewModelItem()
            {
                Amount = 600
            });
            try
            {
                _salaryService.Set(viewModel).GetAwaiter().GetResult();
            }
            catch (ValidationCustomException exception)
            {
                if (exception.AdditionalData.Count == 0)
                {
                    Assert.Fail();
                }
                Assert.IsTrue(true);
                return;
            }
            Assert.Fail();
        }
    }
}
