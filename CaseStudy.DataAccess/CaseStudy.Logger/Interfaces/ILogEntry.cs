﻿using System;

namespace CaseStudy.Logger
{
    public interface ILogEntry
    {
        string ApplicationName { get; }
        object ExtendedInformation { get; }
        DateTime Timestamp { get; }
        string Message { get; }
        string Exception { get; }
        string[] Tags { get; }
    }
}