﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Logger
{
    public interface ILogger
    {
        ILogEntry WriteCritical(string message, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteCritical(string message, Exception ex, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteError(string message, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteError(string message, Exception ex, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteInformation(string message, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteInformation(string message, Exception ex, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteVerbose(string message, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteVerbose(string message, Exception ex, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteWarning(string message, object extendedInformation = null, string[] tags = null);
        ILogEntry WriteWarning(string message, Exception ex, object extendedInformation = null, string[] tags = null);
    }
}
