﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
namespace CaseStudy.Logger
{
    public static class DependencyConfig
    {
        public static IServiceCollection AddLoggerDependencies(this IServiceCollection services, string app,
            IConfiguration configuration)
        {
            var logglyConfigSection = configuration.GetSection("LogglyConfig");
            var inputKey = logglyConfigSection.GetSection("InputKey").ToString();
            var rootUrl = logglyConfigSection.GetSection("RootUrl").ToString();
            var logglyConfig = new LogglyConfig(inputKey, rootUrl);
            services.AddTransient<ILogger, LogglyLogger>((data) =>
            {
                return new LogglyLogger(logglyConfig, app);
            });
            return services;
        }
    }
}
