﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace CaseStudy.Logger
{
    public class LogglyLogger : ILogger
    {
        private bool isValidToken = true;
        private LogglyConfig _config;
        private readonly string _version;
        private readonly string _app;

        public LogglyLogger(LogglyConfig logglyConfig, string app)
        {
            _app = app;
            _config = logglyConfig;
            var version = Assembly.GetCallingAssembly().GetName();
            _version = version.Version.ToString();
        }



        public ILogEntry WriteCritical(string message, Exception ex, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, ex, extendedInformation, tags);
            Send(entry, LogglyLevel.CRITICAL);
            return entry;
        }

        public ILogEntry WriteCritical(string message, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, null, extendedInformation, tags);
            Send(entry, LogglyLevel.CRITICAL);
            return entry;
        }

        public ILogEntry WriteError(string message, Exception ex, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, ex, extendedInformation, tags);
            Send(entry, LogglyLevel.ERROR);
            return entry;
        }

        public ILogEntry WriteError(string message, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, null, extendedInformation, tags);
            Send(entry, LogglyLevel.ERROR);
            return entry;
        }

        public ILogEntry WriteInformation(string message, Exception ex, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, ex, extendedInformation, tags);
            Send(entry, LogglyLevel.INFO);
            return entry;
        }

        public ILogEntry WriteInformation(string message, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, null, extendedInformation, tags);
            Send(entry, LogglyLevel.INFO);
            return entry;
        }

        public ILogEntry WriteVerbose(string message, Exception ex, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, ex, extendedInformation, tags);
            Send(entry, LogglyLevel.INFO);
            return entry;
        }

        public ILogEntry WriteVerbose(string message, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, null, extendedInformation, tags);
            Send(entry, LogglyLevel.INFO);
            return entry;
        }

        public ILogEntry WriteWarning(string message, Exception ex, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, ex, extendedInformation, tags);
            Send(entry, LogglyLevel.WARNING);
            return entry;
        }

        public ILogEntry WriteWarning(string message, object extendedInformation = null, string[] tags = null)
        {
            var entry = CreateLogEntry(message, null, extendedInformation, tags);
            Send(entry, LogglyLevel.WARNING);
            return entry;
        }

        private ILogEntry CreateLogEntry(string message, Exception ex, object extendedInformation, string[] tags)
        {
            var logEntry = new LogEntry(_app, message, ex, extendedInformation, tags);
            return logEntry;
        }

        private void SetTokenValid(bool flag)
        {
            isValidToken = flag;
        }

        private void Send(ILogEntry logEntry, LogglyLevel level)
        {
            if (isValidToken)
            {
                string _tag = _config.Tag;

                //keeping userAgent backward compatible
                if (!string.IsNullOrWhiteSpace(_config.UserAgent))
                {
                    _tag = _tag + "," + _config.UserAgent;
                }
                var body = JsonConvert.SerializeObject(new
                {
                    timestamp = DateTime.UtcNow,
                    level = level.ToString(),
                    thread = _app,
                    logger = _app,
                    message = JsonConvert.SerializeObject(logEntry)
                });
                var bytes = Encoding.UTF8.GetBytes(body);
                var webRequest = CreateWebRequest(_tag);

                using (var dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(bytes, 0, bytes.Length);
                    dataStream.Flush();
                    dataStream.Close();
                }
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                webResponse.Close();
            }
        }

        private HttpWebRequest CreateWebRequest(string tag)
        {
            var url = String.Concat(_config.RootUrl, _config.LogMode, _config.InputKey);
            //adding userAgent as tag in the log
            url = String.Concat(url, "/tag/" + tag);
            HttpWebRequest request = null;
            request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ReadWriteTimeout = request.Timeout = _config.TimeoutInSeconds * 1000;
            request.UserAgent = _config.UserAgent;
            request.KeepAlive = true;
            request.ContentType = "application/json";
            return request;
        }
    }

    public enum LogglyLevel
    {
        INFO,
        WARNING,
        ERROR,
        CRITICAL,
        FATAL
    }

    public class LogEntry : ILogEntry
    {
        public object ExtendedInformation { get; private set; }
        public string ApplicationName { get; private set; }
        public DateTime Timestamp { get; private set; }
        public string[] Tags { get; private set; }
        public string Message { get; private set; }
        public string Exception { get; private set; }

        public LogEntry(string app, string message, Exception exception, object extendedInformation, string[] tags)
        {
            ExtendedInformation = extendedInformation;
            ApplicationName = app;
            Timestamp = DateTime.UtcNow;
            Tags = tags;
            Message = message;
            Exception = JsonConvert.SerializeObject(exception);
        }
    }
}
