﻿using CaseStudy.Logger;
using KingAOP.Aspects;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;

namespace CaseStudy.Utilities.Extensions
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Method)]
    public class TimeItAttribute : OnMethodBoundaryAspect
    {
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly ILogger _logger;
        public string MethodName { get; set; }

        public TimeItAttribute()
        {
            var serviceBuilder = Dependencies.DependencyConfig.ServiceCollection?.BuildServiceProvider();
            if (serviceBuilder == null)
            {
                return;
            }
            _logger = serviceBuilder.GetService<ILogger>();
        }

        public override void OnEntry(MethodExecutionArgs eventArgs)
        {
            _stopwatch.Start();
        }

        public override void OnExit(MethodExecutionArgs eventArgs)
        {
            _stopwatch.Stop();
            double totalMilliSecondsElapsed = _stopwatch.Elapsed.TotalMilliseconds;
            _logger.WriteInformation($"Time It - {MethodName} - {totalMilliSecondsElapsed} miliseconds");
        }
    }
}
