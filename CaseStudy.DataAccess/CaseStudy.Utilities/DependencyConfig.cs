﻿using Microsoft.Extensions.DependencyInjection;

namespace CaseStudy.Utilities.Dependencies
{
    public static class DependencyConfig
    {
        public static IServiceCollection ServiceCollection { get; private set; }

        public static IServiceCollection AddUtilitiesDependencies(this IServiceCollection services)
        {
            ServiceCollection = services;
            return services;
        }
    }
}
