﻿using CaseStudy.DataAccess.Database;
using CaseStudy.DataAccess.Repositories;
using CaseStudy.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.DataAccess.Dependencies
{
    public static class DependencyConfig
    {
        public static IServiceCollection ServiceCollection {  get; private set; }
        public static IServiceCollection AddDataAccessDependencies(this IServiceCollection services, string connectionString)
        {
            ServiceCollection = services;
            services.AddDbContext<ApplicationDbContext>(options =>options.UseSqlServer(connectionString));
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            return services;
        }
    }
}
