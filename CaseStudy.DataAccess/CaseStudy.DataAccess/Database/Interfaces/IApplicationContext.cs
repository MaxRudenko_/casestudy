﻿using CaseStudy.Entities;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace CaseStudy.DataAccess.Database
{
    public interface IApplicationContext
    {
        DatabaseFacade Database { get; }
        DbSet<Salary> Salaries { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        EntityEntry Entry(object entity);
    }
}
