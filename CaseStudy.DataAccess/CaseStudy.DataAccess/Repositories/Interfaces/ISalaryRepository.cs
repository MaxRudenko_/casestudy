﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CaseStudy.Entities;

namespace CaseStudy.DataAccess.Repositories.Interfaces
{
    public interface ISalaryRepository
    {
        Task Insert(ICollection<Salary> salaries);
        Task<ICollection<Salary>> Get();
        Task Remove(Salary id);
        Task Update(Salary salary);
    }
}