﻿using CaseStudy.DataAccess.Database;
using CaseStudy.DataAccess.Repositories.Interfaces;
using CaseStudy.Entities;
using CaseStudy.Logger;
using CaseStudy.Utilities.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseStudy.DataAccess.Repositories
{
    public class SalaryRepository : BaseRepository, ISalaryRepository
    {
        public SalaryRepository(IApplicationContext applicationContext, ILogger logger)
            : base(applicationContext, logger)
        {
        }

        [TimeIt(MethodName = "SalaryRepository.Get")]
        public async Task<ICollection<Salary>> Get()
        {
            var salaries = new List<Salary>();
            await Execute(async () =>
            {
                salaries = _applicationContext.Salaries.ToList();
                await _applicationContext.SaveChangesAsync();
            });
            return salaries;
        }

        [TimeIt(MethodName = "SalaryRepository.Insert")]
        public async Task Insert(ICollection<Salary> salaries)
        {
            await Execute(async () =>
            {
                _applicationContext.Salaries.AddRange(salaries);
                await _applicationContext.SaveChangesAsync();
            });
        }

        [TimeIt(MethodName = "SalaryRepository.Remove")]
        public async Task Remove(Salary salary)
        {
            await Execute(async () =>
            {
                _applicationContext.Salaries.Remove(salary);
                await _applicationContext.SaveChangesAsync();
            });
        }

        [TimeIt(MethodName = "SalaryRepository.Update")]
        public async Task Update(Salary salary)
        {
            await Execute(async () =>
            {
                _applicationContext.Salaries.Attach(salary);
                _applicationContext.Entry(salary).State = EntityState.Modified;
                await _applicationContext.SaveChangesAsync();
            });
        }
    }
}
