﻿using CaseStudy.DataAccess.Database;
using CaseStudy.Logger;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CaseStudy.DataAccess.Repositories
{
    public abstract class BaseRepository
    {
        protected IApplicationContext _applicationContext;
        protected ILogger _logger;

        public BaseRepository(IApplicationContext applicationContext, ILogger logger)
        {
            _applicationContext = applicationContext;
            _logger = logger;
        }

        protected async Task Execute(Func<Task> func)
        {
            try
            {
                await func();
            }
            catch (SqlException exception)
            {
                _logger.WriteCritical(exception.Message,exception);
                throw;
            }
            catch (TimeoutException timeoutException)
            {
                _logger.WriteError(timeoutException.Message, timeoutException);
                throw;
            }
        }
    }
}
