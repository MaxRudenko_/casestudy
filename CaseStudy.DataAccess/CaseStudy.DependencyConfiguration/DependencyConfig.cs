﻿using Microsoft.Extensions.DependencyInjection;
namespace CaseStudy.DependencyConfiguration
{
    public static class DependencyConfig
    {
        public static IServiceCollection Dependencies { get; set; }
        public static IServiceCollection AddDependencies(this IServiceCollection services, string connectionString)
        {
            services.AddSingleton<IApplicationContextSettings>(new ApplicationContextSettings(connectionString));
            services.AddTransient<IApplicationContext, ApplicationContext>();
            services.AddTransient<ILogger, FileLogger>();
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            services.AddTransient<ISalaryService, SalaryService>();
            return services;
        }
    }
}
