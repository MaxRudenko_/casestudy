﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.ViewModels.SalaryViewModels
{
    public class GetSalaryViewModel
    {
        public ICollection<GetSalaryViewModelItem> Salaries { get; set; }
        public GetSalaryViewModel()
        {
            Salaries = new List<GetSalaryViewModelItem>();
        }
    }

    public class GetSalaryViewModelItem
    {
        public long Id { get; set; }
        public double Amount { get; set; }
    }
}
