﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.ViewModels.SalaryViewModels
{
    public class SetSalaryRequestViewModel
    {
        public ICollection<SetSalaryRequestViewModelItem> Salaries { get; set; }

        public SetSalaryRequestViewModel()
        {
            Salaries = new List<SetSalaryRequestViewModelItem>();
        }
    }

    public class SetSalaryRequestViewModelItem
    {
        [Required]
        [Range(0, 99999.99)]
        public double Amount { get; set; }
    }
}
