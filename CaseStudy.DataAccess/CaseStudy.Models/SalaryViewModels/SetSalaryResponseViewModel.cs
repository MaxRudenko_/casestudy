﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.ViewModels.SalaryViewModels
{
    public class SetSalaryResponseViewModel
    {
        public ICollection<SetSalaryRequestViewModelItem> RejectedSalaries { get; set; }

        public SetSalaryResponseViewModel()
        {
            RejectedSalaries = new List<SetSalaryRequestViewModelItem>();
        }
    }

    public class SetSalaryResponseViewModelItem
    {
        public double Amount { get; set; }
    }
}
