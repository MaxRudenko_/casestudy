﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.ViewModels.BaseViewModels
{
    public class BadRequestViewModel
    {
        public string Message { get; set; }
        public Dictionary<string, ModelStateEntry> AdditionalData { get; set; }
    }
}
