using System.Threading.Tasks;
using CaseStudy.Logger;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.ViewModels.AccountViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CaseStudy.Web.Controllers
{
    public class AccountController : BaseController
    {
        private IAccountService _accountService;
        public AccountController(IAccountService accountService,ILogger logger) : base(logger)
        {
            _accountService = accountService;
        }

        public async Task<IActionResult> Login([FromBody]LoginAccountViewModel model)
        {
            return await HandleException(() => _accountService.Login(model));
        }
    }
}
