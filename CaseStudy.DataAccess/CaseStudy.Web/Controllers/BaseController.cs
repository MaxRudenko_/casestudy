﻿using CaseStudy.Logger;
using CaseStudy.Services.Exceptions;
using CaseStudy.ViewModels.BaseViewModels;
using CaseStudy.Web.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CaseStudy.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected ILogger _logger;

        public BaseController(ILogger logger)
        {
            _logger = logger;
        }

        protected virtual async Task<IActionResult> HandleException<T>(Func<Task<T>> function)
        {
            if (!ModelState.IsValid)
            {
                var erorrViewModel = new BadRequestViewModel();
                erorrViewModel.AdditionalData = ModelState.GetErrors();
                return BadRequest(erorrViewModel);
            }
            try
            {
                var result = await function();
                return Ok(result);
            }
            catch (BusinessLogicBaseException exception)
            {
                return Catch(exception);
            }
#if RELEASE
            catch (Exception exception)
            {
                 return Catch(exception);
            }
#endif
        }

        protected virtual async Task<IActionResult> HandleException(Func<Task> function)
        {
            if (!ModelState.IsValid)
            {
                var erorrViewModel = new BadRequestViewModel();
                erorrViewModel.AdditionalData = ModelState.GetErrors();
                return BadRequest(erorrViewModel);
            }
            try
            {
                await function();
                return Ok();
            }
            catch (BusinessLogicBaseException exception)
            {
                return Catch(exception);
            }
#if RELEASE
            catch (Exception exception)
            {
                return Catch(exception);
            }
#endif
        }


        private IActionResult Catch(BusinessLogicBaseException exception )
        {
            _logger.WriteError(exception.Message, exception);
            var erorrViewModel = new BadRequestViewModel();
            erorrViewModel.Message = exception.Message;
            if (exception.AdditionalData != null)
            {
                erorrViewModel.AdditionalData = ModelState.GetErrors(exception.AdditionalData);
            }
            return BadRequest(erorrViewModel);
        }

        private IActionResult Catch(Exception exception)
        {
            _logger.WriteError(exception.Message, exception);
            var erorrViewModel = new BadRequestViewModel();
            erorrViewModel.Message = "Sorry, Something Went Wrong: Please try again later.";
            erorrViewModel.AdditionalData = null;
            return StatusCode(500, erorrViewModel);
        }
    }
}
