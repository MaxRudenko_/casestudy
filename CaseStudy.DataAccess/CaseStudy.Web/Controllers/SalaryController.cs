﻿using CaseStudy.Logger;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.ViewModels.SalaryViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CaseStudy.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SalaryController : BaseController
    {
        private ISalaryService _salaryService;

        public SalaryController(ISalaryService salaryService, ILogger logger): base(logger)
        {
            _salaryService = salaryService;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Set(SetSalaryRequestViewModel salaryViewModel)
        {
            return await HandleException(() => _salaryService.Set(salaryViewModel));
        }
    }
}
