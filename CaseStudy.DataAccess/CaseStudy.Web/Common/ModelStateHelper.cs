﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseStudy.Web.Common
{
    public static class ModelStateHelper
    {
        public static Dictionary<string, ModelStateEntry> GetErrors(this ModelStateDictionary modelState,Dictionary<string,string> additionalData=null)
        {
            var errors = new Dictionary<string, ModelStateEntry>();
            if (additionalData != null)
            {
                foreach(var item in additionalData)
                {
                    modelState.AddModelError(item.Key, item.Value);
                }
            }
            foreach(var item in modelState)
            {
                errors.Add(item.Key, item.Value);
            }
            return errors;
        }
    }
}
