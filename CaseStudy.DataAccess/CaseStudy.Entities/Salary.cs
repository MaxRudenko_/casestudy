﻿using System;

namespace CaseStudy.Entities
{
    public class Salary : BaseEntity
    {
        public double Amount { get; protected set; }

        protected Salary()
        {

        }
    }
}
