﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Entities
{
    public abstract class BaseEntity
    {
        protected bool _immutable = true;
        [Required]
        [Key]
        public long Id { get; set; }

        [Index]
        [Required]
        public DateTime CreationDate
        {
            get;
             protected set;
        }

        protected BaseEntity()
        {
            DateTime now = DateTime.UtcNow;
            CreationDate = new DateTime(now.Ticks / 100000 * 100000, now.Kind);
        }
    }
}
