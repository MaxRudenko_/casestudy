﻿using Microsoft.AspNetCore.Identity;

namespace CaseStudy.Entities
{
    public class ApplicationUser : IdentityUser<int>
    {
    }
}
