﻿using CaseStudy.Entities;
using CaseStudy.Services.Validators.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Services.Validators
{
    internal class SalaryValidator : ISalaryValidator
    {
        public const double MaxSalary = 500;

        public SalaryValidator()
        {

        }

        public IValidatorErrors Validate(Salary entity)
        {
            var errors = new ValidatorErrors();
            if (entity.Amount > MaxSalary)
            {
                errors.Errors.Add("Amount", $"Salary:{entity.Amount} is too large!");
            }
            return errors;
        }
    }
}
