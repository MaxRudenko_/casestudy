﻿using CaseStudy.Entities;
using CaseStudy.Services.Validators.Interfaces;

namespace CaseStudy.Validators.Validators
{
    public interface ISalaryValidator: IValidator<Salary>
    {
    }
}