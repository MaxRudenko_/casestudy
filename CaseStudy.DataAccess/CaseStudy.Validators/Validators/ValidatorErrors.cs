﻿using CaseStudy.Services.Validators.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Services.Validators
{
    internal class ValidatorErrors : IValidatorErrors
    {
        public Dictionary<string, string> Errors { get; set; }

        public ValidatorErrors()
        {
            Errors = new Dictionary<string, string>();
        }
    }
}
