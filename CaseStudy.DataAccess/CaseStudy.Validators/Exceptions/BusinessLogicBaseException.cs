﻿using System;
using System.Collections.Generic;

namespace CaseStudy.Validators.Exceptions
{
    public abstract class BusinessLogicBaseException : Exception
    {
        public readonly Dictionary<string,string> AdditionalData;

        public BusinessLogicBaseException(string message, Dictionary<string, string> additionalData) : base(message)
        {
            AdditionalData = additionalData;
        }

        public BusinessLogicBaseException(string message, Exception innerException, object additionalData) : base(message, innerException)
        {

        }
    }
}
