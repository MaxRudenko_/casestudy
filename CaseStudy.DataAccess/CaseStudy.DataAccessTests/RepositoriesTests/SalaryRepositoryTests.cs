﻿using System;
using System.Collections.Generic;
using System.Linq;
using CaseStudy.DataAccess.Database;
using CaseStudy.DataAccess.Repositories.Interfaces;
using CaseStudy.Entities;
using CaseStudy.Services.Builders;
using CaseStudy.Services.Validators;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CaseStudy.DataAccessTests
{
    [TestClass]
    public class SalaryRepositoryTests
    {
        private ISalaryRepository _salaryRepository;
        private ISalaryValidator _salaryValidator;
        private IApplicationContext _applicationContext;
        public SalaryRepositoryTests()
        {
            var services = DependencyConfig.GetMockDependencies();
            var serviceProvider = services.BuildServiceProvider();
            _applicationContext = serviceProvider.GetService<IApplicationContext>();
            _salaryRepository = serviceProvider.GetService<ISalaryRepository>();
            _salaryValidator = serviceProvider.GetService<ISalaryValidator>();
        }

        [TestMethod]
        public void InsertTest()
        {
            using (var dbContextTransaction = _applicationContext.Database.BeginTransaction())
            {
                try
                {
                    var salaries = new List<Salary>
                    {
                         new SalaryBuilder(_salaryValidator).SetAmount(200).Build(),
                        new SalaryBuilder(_salaryValidator).SetAmount(500).Build(),
                    };
                    _salaryRepository.Insert(salaries);
                }
                catch (Exception ex)
                {
                    Assert.Fail();
                }
                finally
                {
                    dbContextTransaction.Rollback();
                }
            }
        }

        [TestMethod]
        public void RemoveTest()
        {
            using (var dbContextTransaction = _applicationContext.Database.BeginTransaction())
            {
                try
                {
                    var salaries = new List<Salary>
                    {
                         new SalaryBuilder(_salaryValidator).SetAmount(200).Build(),
                        new SalaryBuilder(_salaryValidator).SetAmount(500).Build(),
                    };
                    _salaryRepository.Insert(salaries).GetAwaiter().GetResult();
                    _salaryRepository.Remove(salaries[0]).GetAwaiter().GetResult();
                    ICollection<Salary> result = _salaryRepository.Get().GetAwaiter().GetResult();
                    if (result.Count == salaries.Count)
                    {
                        Assert.Fail();
                    }
                }
                catch (Exception ex)
                {
                    Assert.Fail();
                }
                finally
                {
                    dbContextTransaction.Rollback();
                }
            }
        }

        [TestMethod]
        public void GetTest()
        {
            using (var dbContextTransaction = _applicationContext.Database.BeginTransaction())
            {
                try
                {
                    var salaries = new List<Salary>
                    {
                         new SalaryBuilder(_salaryValidator).SetAmount(200).Build(),
                        new SalaryBuilder(_salaryValidator).SetAmount(500).Build(),
                    };
                    _salaryRepository.Insert(salaries).GetAwaiter().GetResult();
                    ICollection<Salary> result = _salaryRepository.Get().GetAwaiter().GetResult();
                    if (result.Where(entity => entity.Id == salaries[0].Id || entity.Id != salaries[1].Id).Count() == salaries.Count())
                    {
                        Assert.Fail();
                    }
                }
                catch (Exception ex)
                {
                    Assert.Fail();
                }
                finally
                {
                    dbContextTransaction.Rollback();
                }
            }
        }

        [TestMethod]
        public void UpdateTest()
        {
            using (var dbContextTransaction = _applicationContext.Database.BeginTransaction())
            {
                try
                {
                    var salaries = new List<Salary>
                    {
                         new SalaryBuilder(_salaryValidator).SetAmount(200).Build(),
                        new SalaryBuilder(_salaryValidator).SetAmount(500).Build(),
                    };
                    _salaryRepository.Insert(salaries).GetAwaiter().GetResult();
                    salaries[0] = new SalaryBuilder(_salaryValidator, salaries[0]).SetAmount(200).Build();
                    _salaryRepository.Update(salaries[0]).GetAwaiter().GetResult();
                }
                catch (Exception ex)
                {
                    Assert.Fail();
                }
                finally
                {
                    dbContextTransaction.Rollback();
                }
            }
        }
    }
}
