﻿using CaseStudy.DataAccess.Database;
using CaseStudy.DataAccess.Dependencies;
using CaseStudy.DataAccess.Repositories.Interfaces;
using CaseStudy.Entities;
using CaseStudy.Logger;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.ViewModels.SalaryViewModels;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace CaseStudy.DataAccessTests
{
    public static class DependencyConfig
    {
        public static IServiceCollection GetMockDependencies()
        {
            var services = new ServiceCollection();
            var connectionString = ConfigurationManager.ConnectionStrings["MockConnection"].ConnectionString;
            services.AddDataAccessDependencies(connectionString);
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(x => x.WriteCritical(It.IsAny<string>(), It.IsAny<Exception>(), null, null));
            mockLogger.Setup(x => x.WriteError(It.IsAny<string>(),It.IsAny<Exception>(),null,null));
            mockLogger.Setup(x => x.WriteInformation(It.IsAny<string>(), null, null));
            services.AddSingleton(mockLogger.Object);
            return services;
        }
    }
}
