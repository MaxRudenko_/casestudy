﻿using CaseStudy.DataAccess.Dependencies;
using CaseStudy.Logger;
using CaseStudy.Services.Services;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.Services.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace CaseStudy.Services
{
    public static class DependencyConfig
    {
        public static IServiceCollection ServiceCollection { get; private set; }

        public static IServiceCollection AddDataAccessDependencies(this IServiceCollection services, string connectionString)
        {
            ServiceCollection = services;
            services.AddDataAccessDependencies(connectionString);
            return services;
        }

        public static IServiceCollection AddServicesDependencies(this IServiceCollection services)
        {
            ServiceCollection = services;
            services.AddTransient<ISalaryService, SalaryService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<ISalaryValidator, SalaryValidator>();
            return services;
        }
    }
}
