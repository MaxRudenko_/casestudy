﻿using CaseStudy.Entities;
using CaseStudy.Services.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Services.Builders
{
    public class SalaryBuilder : Salary
    {
        private ISalaryValidator _salaryValidator;
        public SalaryBuilder(ISalaryValidator validator, Salary salary = null) : base()
        {
            _salaryValidator = validator;
            if (salary == null)
            {
                return;
            }
            Amount = salary.Amount;

            CreationDate = salary.CreationDate;
            Id = salary.Id;
        }

        public SalaryBuilder SetAmount(double amount)
        {
            Amount = amount;
            return this;
        }

        public SalaryBuilder SetDate(DateTime dateTime)
        {
            CreationDate = dateTime;
            return this;
        }

        public Salary Build()
        {
            _salaryValidator.Validate(this);
            return this;
        }
    }
}
