﻿using CaseStudy.Entities;
using CaseStudy.Services.Validators.Interfaces;

namespace CaseStudy.Services.Validators
{
    public interface ISalaryValidator: IValidator<Salary>
    {
    }
}