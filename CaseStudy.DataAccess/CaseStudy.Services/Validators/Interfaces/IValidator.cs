﻿using CaseStudy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseStudy.Services.Validators.Interfaces
{
    public interface IValidator<T> where T : BaseEntity
    {
        void Validate(T entity);
    }
}
