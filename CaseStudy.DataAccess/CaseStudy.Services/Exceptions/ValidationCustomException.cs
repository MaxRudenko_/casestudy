﻿using System;
using System.Collections.Generic;

namespace CaseStudy.Services.Exceptions
{
    public class ValidationCustomException : BusinessLogicBaseException
    {
        public ValidationCustomException(string message, Dictionary<string, string> additionalData) : base(message, additionalData)
        {

        }

        public ValidationCustomException(string message, Exception innerException, object additionalData) : base(message, innerException, additionalData)
        {

        }
    }
}
