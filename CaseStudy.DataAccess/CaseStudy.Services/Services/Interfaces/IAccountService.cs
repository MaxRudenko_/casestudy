﻿using System.Threading.Tasks;
using CaseStudy.ViewModels.AccountViewModels;

namespace CaseStudy.Services.Services.Interfaces
{
    public interface IAccountService
    {
        Task<string> Login(LoginAccountViewModel model);
    }
}