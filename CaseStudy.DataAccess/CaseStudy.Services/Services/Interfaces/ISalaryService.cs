﻿using CaseStudy.ViewModels.SalaryViewModels;
using System.Threading.Tasks;

namespace CaseStudy.Services.Services.Interfaces
{
    public interface ISalaryService
    {
        Task Set(SetSalaryRequestViewModel salaryViewModel);
    }
}