﻿using CaseStudy.DataAccess.Repositories.Interfaces;
using CaseStudy.Entities;
using CaseStudy.Logger;
using CaseStudy.Services.Builders;
using CaseStudy.Services.Exceptions;
using CaseStudy.Services.Services.Interfaces;
using CaseStudy.Services.Validators;
using CaseStudy.Services.Validators.Interfaces;
using CaseStudy.Utilities.Extensions;
using CaseStudy.ViewModels.SalaryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseStudy.Services.Services
{
    public class SalaryService : ISalaryService
    {
        private ISalaryRepository _salaryRepository;
        private ILogger _logger;
        private ISalaryValidator _salaryValidator;

        public SalaryService(ISalaryRepository salaryRepository, ILogger logger, ISalaryValidator salaryValidator)
        {
            _salaryValidator = salaryValidator;
            _salaryRepository = salaryRepository;
            _logger = logger;
        }

        public async Task<GetSalaryViewModel> Get()
        {
            var viewModel = new GetSalaryViewModel();
            ICollection<Salary> salaries = await _salaryRepository.Get();
            viewModel.Salaries = salaries.Select(entity => new GetSalaryViewModelItem
            {
                Amount = entity.Amount,
                Id = entity.Id
            }).ToList();
            return viewModel;
        }

        public async Task Set(SetSalaryRequestViewModel salaryViewModel)
        {
            var salaries = new List<Salary>();
            var additionalData = new Dictionary<string, string>();
            foreach (var item in salaryViewModel.Salaries)
            {
                try
                {
                   Salary salary = new SalaryBuilder(_salaryValidator)
                          .SetDate(DateTime.UtcNow)
                          .SetAmount(item.Amount)
                          .Build();
                    salaries.Add(salary);
                }
                catch (ValidationCustomException exception)
                {
                    additionalData.AddRange(exception.AdditionalData);
                }
            }
            if (salaries.Count > 0)
            {
                await _salaryRepository.Insert(salaries);
            }
            if (additionalData.Count > 0)
            {
                throw new ValidationCustomException("Some salaries are too large!", additionalData);
            }
        }
    }
}
